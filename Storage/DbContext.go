package Storage

import (
	// "encoding/hex"
	"errors"
	"github.com/dgraph-io/badger"
	proto "github.com/gogo/protobuf/proto"
	//"github.com/golang/glog"
	"log"
	"sync"
)

var opts badger.Options
var dbInstance *badger.DB

func init() {
	// Open the Badger database located in the /tmp/badger directory.
	// It will be created if it doesn't exist.
	opts = badger.DefaultOptions
	opts.Dir = "./badger"
	opts.ValueDir = "./badger"
	// Your code here…
}

func CloseDb() {
	if dbInstance != nil {
		dbInstance.Close()
	}
}

func getDb() *badger.DB {
	if dbInstance == nil {
		db, err := badger.Open(opts)
		if err != nil {
			log.Fatal(err)
		}
		dbInstance = db
	}
	return dbInstance
}

func checkErr(e error) {
	if e != nil {
		panic(e)
	}
}

var mutex = sync.Mutex{}

// Set sets a record in the database
func Set(hash []byte, obj proto.Message) {
	mutex.Lock()
	defer mutex.Unlock()

	db := getDb()
	e := db.Update(func(txn *badger.Txn) error {
		b, e := proto.Marshal(obj)
		//glog.Infof("setting %s to %s", hex.EncodeToString(hash), hex.EncodeToString(b))
		checkErr(e)
		e = txn.Set(hash, b)
		checkErr(e)
		return nil
	})
	checkErr(e)
}

// Delete deletes a record in the database
func Delete(hash []byte) {
	mutex.Lock()
	defer mutex.Unlock()

	db := getDb()
	e := db.Update(func(txn *badger.Txn) error {
		e := txn.Delete(hash)
		checkErr(e)
		return nil
	})
	checkErr(e)
}

// Read gives access to the proto.Message that is stored at a
// given key
func Read(hash []byte, out proto.Message) (e error) {
	db := getDb()
	err := db.View(func(txn *badger.Txn) error {
		item, err := txn.Get(hash)
		if err != nil {
			return err
		}
		val, err := item.Value()
		if err != nil {
			return err
		}
		if val == nil {
			return errors.New("nil")
		}
		var toReturn = out
		err = proto.Unmarshal(val, toReturn)
		return err
	})
	return err
}
