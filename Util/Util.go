package Util

import (
	"../Crypto"
	"../Protobufs"
)

// HashBlock hashes a block
func HashBlock(block *Protobufs.ProofOfWorkBlock) []byte {
	toHash := append(block.Nonce, block.PubKey...)
	toHash = append(toHash, block.PreviousBlock...)
	return Crypto.SafeHash(toHash)
}
