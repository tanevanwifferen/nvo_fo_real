package Networking

import (
	"../Blockchain"
	"../Protobufs"
	"github.com/gogo/protobuf/proto"
	"github.com/golang/glog"
	"github.com/perlin-network/noise/crypto/ed25519"
	"github.com/perlin-network/noise/network"
	"github.com/perlin-network/noise/network/backoff"
	"github.com/perlin-network/noise/network/discovery"
	"github.com/perlin-network/noise/network/nat"
)

// InitMessagingServer connects to peers, and accept incoming connections at
func InitMessagingServer(
	addr string,
	peers []string,
	toSendStream <-chan proto.Message,
	plugin *BlockchainPlugin,
	doneChan chan bool,
) {
	keys := ed25519.RandomKeyPair()

	glog.Infof("Private Key: %s", keys.PrivateKeyHex())
	glog.Infof("Public Key: %s", keys.PublicKeyHex())

	builder := network.NewBuilder()
	builder.SetKeys(keys)
	builder.SetAddress(addr)

	// Register peer discovery plugin.
	_ = builder.AddPlugin(new(discovery.Plugin))
	builder.AddPlugin(new(backoff.Plugin))

	// Enables automated NAT traversal/port forwarding for your node.
	// Check documentation for more info.
	nat.RegisterPlugin(builder)

	// Add custom chat plugin.
	_ = builder.AddPlugin(plugin)

	net, err := builder.Build()
	if err != nil {
		glog.Fatal(err)
		return
	}

	go net.Listen()

	if len(peers) > 0 {
		glog.Infof("bootstrapping with %s", peers[0])
		net.Bootstrap(peers...)
	}

	sendBlocksOverNetwork(net)

	doneChan <- true
	for {
		packet := <-toSendStream
		net.Broadcast(packet)
	}
}

func sendBlocksOverNetwork(network *network.Network) int {
	channle := make(chan *Protobufs.ProofOfWorkBlock)
	go func() {
		for {
			packet := <-channle
			network.Broadcast(packet)
		}
	}()
	toReturn := Blockchain.ListenToBlockUpdates(channle)
	return toReturn
}
