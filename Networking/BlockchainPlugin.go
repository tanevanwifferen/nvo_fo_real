package Networking

import (
	"../Blockchain"
	"../Mempool"
	"../Protobufs"
	"encoding/hex"
	"github.com/golang/glog"
	"github.com/perlin-network/noise/network"
	"strings"
)

// BlockchainPlugin is a plugin that enables chat functionality over p2p
type BlockchainPlugin struct {
	*network.Plugin
}

// Receive is an event handler for when a message comes in
func (p *BlockchainPlugin) Receive(ctx *network.PluginContext) error {
	switch x := ctx.Message().(type) {
	case *Protobufs.Transaction:
		glog.Infof("Received message %s from %s", x.Data, hex.EncodeToString(x.Hash))
		err := Mempool.AddTx(x)
		if err == nil {
			ctx.Network().Broadcast(x)
		}
	case *Protobufs.ProofOfWorkBlock:
		glog.Infof("Received new block %s with height %d", hex.EncodeToString(x.Hash), x.BlockNumber)
		/*
			if !Blockchain.VerifyBlock(x) {
				glog.Infof("block %s not added", hex.EncodeToString(x.Hash))
				return nil
			}
		*/
		glog.Infof("block %s is valid", hex.EncodeToString(x.Hash))
		err := Blockchain.InsertBlock(x)
		if err != nil {
			glog.Info(err)
			return err
		}
		for _, tx := range x.Transactions {
			Mempool.RemoveTx(tx.Hash, true)
		}
		Blockchain.CheckOrphanStatus(ctx, x)
		latestBlock, err := Blockchain.GetLatestBlock()
		if err == nil {
			if latestBlock.BlockNumber > x.BlockNumber {
				ctx.Reply(latestBlock)
			}
		}
		glog.Infof("received block %s", hex.EncodeToString(x.Hash))
	case *Protobufs.StringWrapper:
		message := x.Message
		glog.Info("stringwrapper arrived")
		if message == "maxblockheight" {
			glog.Info("request received to get max block height")
			node, err := Blockchain.CalcBestLeafNode()
			if err != nil {
				return err
			}
			ctx.Reply(node)
		}
		if strings.HasPrefix(message, "getblock") {
			id := strings.TrimPrefix(message, "getblock")
			var hash, err = hex.DecodeString(id)
			glog.Info("request received to get block " + id)
			if err != nil {
				return err
			}

			block, err := Blockchain.GetBlock(hash)
			if err == nil {
				ctx.Reply(block)
			}
		}
	}

	return nil
}

// PeerConnect is an event handler for when a new connection is made
func (*BlockchainPlugin) PeerConnect(client *network.PeerClient) {
	latest, err := Blockchain.GetLatestBlock()
	if err == nil {
		client.Tell(latest)
	} else {
		glog.Error(err)
	}
}
