package main

import (
	"./Blockchain"
	"./Miner"
	"./Networking"
	"./Storage"
	"./Wallet"
	"./Website"
	"flag"
	"github.com/asticode/go-astilectron"
	"github.com/gogo/protobuf/proto"
	"github.com/golang/glog"
	"github.com/perlin-network/noise/network"
	"strconv"
	"strings"
	"time"
)

var (
	portFlag         = flag.Int("port", 3000, "port to listen to")
	electronPortFlag = flag.Int("rpcport", 8081, "port to set electron to listen to")
	hostFlag         = flag.String("host", "localhost", "host to listen to")
	protocolFlag     = flag.String("protocol", "tcp", "protocol to use (kcp/tcp)")
	peersFlag        = flag.String("peers", "", "peers to connect to")
	genesisFlag      = flag.Bool("genesis", false, "start a new genesis if blockchain is empty")
	mining           = false
	outChan          = make(chan proto.Message)
)

func startUpNetworking(plugin *Networking.BlockchainPlugin, doneChan chan bool) {
	port := uint16(*portFlag)
	host := *hostFlag
	protocol := *protocolFlag
	peers := strings.Split(*peersFlag, ",")

	listeningAddress := network.FormatAddress(protocol, host, port)

	Networking.InitMessagingServer(listeningAddress, peers, outChan, plugin, doneChan)
}

func main() {
	// glog defaults to logging to a file, override this flag to log to console for testing
	_ = flag.Set("logtostderr", "true")

	flag.Parse()

	// debug := flag.Bool("debug", false, "enables the debug mode")
	g := &Wallet.Wallet{}
	var wallet **Wallet.Wallet = &g

	// start website
	electronPort := strconv.Itoa(*electronPortFlag)
	Website.StartWebsite(wallet, electronPort, outChan)

	// Initialize astilectron
	var a, _ = astilectron.New(astilectron.Options{
		AppName:            "nov",
		AppIconDefaultPath: "eye.png",           // If path is relative, it must be relative to the data directory
		AppIconDarwinPath:  "<your .icns icon>", // Same here
		BaseDirectoryPath:  "vendor/astielectron",
	})
	defer a.Close()

	// Start astilectron
	a.Start()
	// Create a new window
	var w, _ = a.NewWindow("http://127.0.0.1:"+electronPort+"/site/index.html", &astilectron.WindowOptions{
		Center: astilectron.PtrBool(true),
		Height: astilectron.PtrInt(600),
		Width:  astilectron.PtrInt(600),
	})
	w.Create()
	// w.OpenDevTools()

	for {
		time.Sleep(1 * time.Second)
		if len((**wallet).GetPubKey()) != 0 {
			break
		}
	}
	glog.Info("wallet initialized")

	plugin := &Networking.BlockchainPlugin{}
	networkingFinished := make(chan bool)
	go startUpNetworking(plugin, networkingFinished)
	<-networkingFinished

	_, err := Blockchain.GetLatestBlock()
	if *genesisFlag && err != nil {
		block := Miner.MineRootBlock(*wallet, Blockchain.InitDifficulty)
		Blockchain.InsertBlock(block)
	}

	// Blocking pattern
	a.Wait()

	Storage.CloseDb()
}
