package Crypto

import (
	// "bufio"
	// "encoding/hex"
	// "fmt"
	"github.com/tyler-smith/go-bip39"
	"sync"
	// "os"
)

var hashLock = sync.Mutex{}

// GetLock returns a lock used in accessing the crypto library. Unfortunately it
// has an internal state, which requires this lock
func GetLock() *sync.Mutex {
	return &hashLock
}

// Safehash hashes using the lock
func SafeHash(toHash []byte) []byte {
	hashLock.Lock()
	defer hashLock.Unlock()
	return Secp256k1Hash(toHash)
}

// SafeSign signs using the lock
func SafeSign(hash []byte, privkey []byte) []byte {
	hashLock.Lock()
	defer hashLock.Unlock()
	return Sign(hash, privkey)
}

// SafeVerifySignature signs using the lock
func SafeVerifySignature(hash []byte, signature []byte, pubkey []byte) int {
	hashLock.Lock()
	defer hashLock.Unlock()
	return VerifySignature(hash, signature, pubkey)
}

// GetMnemonic returns a string used to generate hd keypairs
func GetMnemonic() string {
	// Generate a mnemonic for memorization or user-friendly seeds
	entropy, _ := bip39.NewEntropy(256)
	mnemonic, _ := bip39.NewMnemonic(entropy)
	return mnemonic
}

// GetSeed converts a mnemonic and passphrase to a seed
func GetSeed(mnemonic string, passphrase string) []byte {
	seed := bip39.NewSeed(mnemonic, passphrase)
	return seed
}

// GetKeyPair returns a private and public key to sign messages
// with
func GetKeyPair(seed []byte) ([]byte, []byte) {
	hashLock.Lock()
	defer hashLock.Unlock()
	return GenerateDeterministicKeyPair(seed)
}
