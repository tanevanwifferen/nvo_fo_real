package Website

import (
	"../Protobufs"
	"encoding/hex"
)

type block struct {
	Hash          string
	SignatureHash string
	Nonce         string
	Signature     string
	PubKey        string
	PreviousBlock string
	BlockNumber   uint64
	NextBlock     []string
	Timestamp     uint64
	Difficulty    uint64
	Transactions  []*tx
}

func blockToWebBlock(b *Protobufs.ProofOfWorkBlock) *block {
	txs := make([]*tx, 0)
	for _, transaction := range b.Transactions {
		txs = append(txs, transactionToTx(transaction))
	}

	nextBlocks := make([]string, 0)
	for _, next := range b.NextBlock {
		nextBlocks = append(nextBlocks, hex.EncodeToString(next))
	}
	toReturn := block{
		Hash:          hex.EncodeToString(b.Hash),
		SignatureHash: hex.EncodeToString(b.SignatureHash),
		Nonce:         hex.EncodeToString(b.Nonce),
		Signature:     hex.EncodeToString(b.Signature),
		PubKey:        hex.EncodeToString(b.PubKey),
		PreviousBlock: hex.EncodeToString(b.PreviousBlock),
		BlockNumber:   b.BlockNumber,
		NextBlock:     nextBlocks,
		Timestamp:     b.Timestamp,
		Difficulty:    b.Difficulty,
		Transactions:  txs,
	}
	return &toReturn
}

type tx struct {
	Hash      string
	Pubkey    string
	Signature string
	Data      string
	Inputs    []*input
	Outputs   []*output
	Timestamp uint64
}

func transactionToTx(transaction *Protobufs.Transaction) *tx {
	inputs := make([]*input, 0)
	for _, input := range transaction.Inputs {
		inputs = append(inputs, inputToWebInput(input))
	}
	outputs := make([]*output, 0)
	for _, output := range transaction.Outputs {
		outputs = append(outputs, outputToWebOutput(output))
	}
	toReturn := tx{
		Hash:      hex.EncodeToString(transaction.Hash),
		Pubkey:    hex.EncodeToString(transaction.Pubkey),
		Signature: hex.EncodeToString(transaction.Signature),
		Data:      hex.EncodeToString(transaction.Data),
		Inputs:    inputs,
		Outputs:   outputs,
		Timestamp: transaction.Timestamp,
	}
	return &toReturn
}

type input struct {
	InputQuery *query
	Password   string
}

func webInputToInput(i *input) *Protobufs.Input {
	toReturn := Protobufs.Input{
		InputQuery: webQueryToQuery(*i.InputQuery),
		Password:   []byte(i.Password),
	}
	return &toReturn
}

func inputToWebInput(i *Protobufs.Input) *input {
	toReturn := input{
		InputQuery: queryToWebQuery(i.InputQuery),
		Password:   string(i.Password),
	}
	return &toReturn
}

type output struct {
	Pubkey       string
	Amount       uint64
	PasswordHash string
	RevertTime   uint64
}

func outputToWebOutput(i *Protobufs.Output) *output {
	toReturn := output{
		Pubkey:       hex.EncodeToString(i.Pubkey),
		Amount:       i.Amount,
		PasswordHash: hex.EncodeToString(i.PasswordHash),
		RevertTime:   i.RevertTime,
	}
	return &toReturn
}

type query struct {
	TransactionHash string
	OutputNumber    uint32
}

func queryToWebQuery(q *Protobufs.Query) *query {
	toReturn := query{
		TransactionHash: hex.EncodeToString(q.TransactionHash),
		OutputNumber:    q.OutputNumber,
	}
	return &toReturn
}

func webQueryToQuery(q query) *Protobufs.Query {
	hash, err := hex.DecodeString(q.TransactionHash)
	if err != nil {
		panic(err)
	}

	toReturn := Protobufs.Query{
		TransactionHash: hash,
		OutputNumber:    q.OutputNumber,
	}
	return &toReturn
}
