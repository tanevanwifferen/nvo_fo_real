package Website

import (
	"../Blockchain"
	"../Crypto"
	"../Mempool"
	"../Miner"
	"../Protobufs"
	"../Wallet"
	"encoding/hex"
	"encoding/json"
	"github.com/golang/glog"
	"io"
	"net/url"
	"strconv"
)

// APIHandler is a struct that contains all exported api functions
type APIHandler struct {
	Wallet **Wallet.Wallet
}

// MinerRunningAction returns a value indicating whether the miner is runnign
func (*APIHandler) MinerRunningAction(interface{}) bool {
	running := Miner.MinerRunning()
	return running
}

// GetUtxosAction returns a value indicating whether the miner is running
func (state *APIHandler) GetUtxosAction(interface{}) []*utxo {
	utxos := (*state.Wallet).GetUTXOs()
	tosend := make([]*utxo, 0)
	for _, utxo := range utxos {
		tosend = append(tosend, newUtxo(utxo))
	}
	return tosend
}

// CreateMnemonicAction returns a new mnemonic
func (*APIHandler) CreateMnemonicAction(interface{}) string {
	return Crypto.GetMnemonic()
}

// GetLatestBlockAction returns the latest block
func (*APIHandler) GetLatestBlockAction(interface{}) *block {
	latest, err := Blockchain.GetLatestBlock()
	if err != nil {
		panic(err)
	}
	return blockToWebBlock(latest)
}

// GetBlockAction returns a block with given hash
func (*APIHandler) GetBlockAction(q url.Values) *block {
	hashParsed, err := hex.DecodeString(q["hash"][0])
	if err != nil {
		panic(err)
	}
	foundBlock, err := Blockchain.GetBlock(hashParsed)
	if err != nil {
		panic(err)
	}
	return blockToWebBlock(foundBlock)
}

// GetTransactionAction returns a transaction by hash
func (*APIHandler) GetTransactionAction(q url.Values) *tx {
	hashParsed, err := hex.DecodeString(q["hash"][0])
	if err != nil {
		panic(err)
	}
	foundTransaction, _, err := Blockchain.GetTransaction(hashParsed)
	if err != nil {
		panic(err)
	}
	return transactionToTx(foundTransaction)
}

// GetBlocksAction returns a set of blocks, length of 100, with offset 100*n
func (*APIHandler) GetBlocksAction(q url.Values) []*block {
	page, ok := q["page"]
	if !ok {
		page = []string{"0"}
	}
	pageInt, err := strconv.Atoi(page[0])
	if err != nil {
		panic("can't parse page")
	}

	startIndex := 100 * pageInt
	endIndex := 100 * (pageInt + 1)
	currentIndex := 0

	var blockToCheck *Protobufs.ProofOfWorkBlock
	blocks := make([]*block, 0)
	for {
		if blockToCheck == nil {
			blockToCheck, err = Blockchain.GetLatestBlock()
		} else {
			blockToCheck, err = Blockchain.GetPreviousBlock(blockToCheck)
		}
		currentIndex++
		if err != nil {
			break
		}
		if currentIndex > startIndex && currentIndex <= endIndex {
			blocks = append(blocks, blockToWebBlock(blockToCheck))
		}
		if currentIndex > endIndex {
			break
		}
	}
	return blocks
}

// GetHistoryAction returns a block with given hash
func (state *APIHandler) GetHistoryAction(q url.Values) []*tx {
	history, err := (*state.Wallet).FetchTransactions()
	if err != nil {
		panic(err)
	}
	tosend := make([]*tx, 0)
	for _, transaction := range history {
		tosend = append(tosend, transactionToTx(transaction))
	}
	return tosend
}

// StartMinerPostAction starts the miner process
func (*APIHandler) StartMinerPostAction(form url.Values, body io.ReadCloser) bool {
	Miner.StartMiner(*apiHandler.Wallet)
	return true
}

// StopMinerPostAction stops the miner process
func (*APIHandler) StopMinerPostAction(form url.Values, body io.ReadCloser) bool {
	Miner.StopMiner()
	return true
}

// LoginPostAction logs a user in with given password and mnemonic
func (state *APIHandler) LoginPostAction(form url.Values, body io.ReadCloser) string {
	glog.Info("login")
	glog.Info("form")
	mnemonic := form["mnemonic"][0]
	pwd := form["password"][0]
	wal, e := Wallet.NewWallet(mnemonic, pwd)
	if e != nil {
		panic(e)
	}
	*state.Wallet = wal
	// *wallet = wal

	return hex.EncodeToString(wal.GetPubKey())
}

// RevertLockTransactionPostAction reverts a transaction
func (state *APIHandler) RevertLockTransactionPostAction(form url.Values, body io.ReadCloser) bool {
	decoder := json.NewDecoder(body)
	var t input
	err := decoder.Decode(&t)

	glog.Info("parsing input")
	glog.Info(t)

	if err != nil {
		panic(err)
	}

	i := webInputToInput(&t)

	oldOutput, err := Blockchain.GetUTXO(i.InputQuery)
	if err != nil {
		panic(err)
	}

	amount := oldOutput.Amount

	output := Protobufs.Output{
		Pubkey: (*state.Wallet).GetPubKey(),
		Amount: amount,
	}

	tx := (*state.Wallet).CreateTransaction([]*Protobufs.Input{i}, []*Protobufs.Output{&output})

	if v, _ := Blockchain.VerifyTransaction(tx, false); !v {
		panic("transaction invalid")
	}
	glog.Infof("creating transaction done, adding to mempool")
	error := Mempool.AddTx(tx)
	glog.Infof("mempool add done, broadcasting to net")
	if error == nil {
		outputChannle <- tx
	} else {
		panic(error)
	}
	return true
}

// SendUnlockTransactionPostAction Unlocks a transaction, and sends coins to own address
func (state *APIHandler) SendUnlockTransactionPostAction(form url.Values, body io.ReadCloser) bool {
	decoder := json.NewDecoder(body)
	var t input
	err := decoder.Decode(&t)

	glog.Info("parsing input")
	glog.Info(t)

	if err != nil {
		panic(err)
	}

	i := webInputToInput(&t)
	glog.Info(i)

	tx, err := (*state.Wallet).CreateUnlockTransaction(i)
	if err != nil {
		panic(err)
	}
	if v, _ := Blockchain.VerifyTransaction(tx, false); !v {
		panic("transaction invalid")
	}
	glog.Infof("creating transaction done, adding to mempool")
	error := publishTransaction(tx)
	if error != nil {
		glog.Info(error)
		return false
	}
	return true
}

func publishTransaction(tx *Protobufs.Transaction) error {
	if v, _ := Blockchain.VerifyTransaction(tx, false); !v {
		panic("transaction invalid")
	}
	error := Mempool.AddTx(tx)
	glog.Infof("mempool add done, broadcasting to net")
	if error == nil {
		outputChannle <- tx
		return nil
	}
	panic(error)
}

// SendTransactionPostAction sends a transaction
func (state *APIHandler) SendTransactionPostAction(form url.Values, body io.ReadCloser) bool {
	decoder := json.NewDecoder(body)
	var t inputoutputset
	err := decoder.Decode(&t)

	glog.Info("parsing input")
	glog.Info(t)

	if err != nil {
		panic(err)
	}

	inputs := make([]*Protobufs.Input, 0)
	for _, input := range t.Inputs {
		glog.Info("parsing input")
		glog.Info(input)
		transaction, err := hex.DecodeString(input.InputQuery.Txhash)
		if err != nil {
			panic(err)
		}
		q := Protobufs.Query{
			OutputNumber:    input.InputQuery.OutputN,
			TransactionHash: transaction,
		}

		passwd, err := hex.DecodeString(input.Password)
		if err != nil {
			panic(err)
		}
		i := Protobufs.Input{
			InputQuery: &q,
			Password:   passwd,
		}
		inputs = append(inputs, &i)
		glog.Infof("added input, of %d", input.InputQuery.Amount)
	}

	outputs := make([]*Protobufs.Output, 0)
	for _, output := range t.Outputs {
		glog.Info("parsing output")
		glog.Info(output)
		pk, err := hex.DecodeString(output.Pubkey)
		if err != nil {
			panic(err)
		}
		o := Protobufs.Output{
			Pubkey:     pk,
			Amount:     output.Amount,
			RevertTime: output.RevertTime,
		}
		if t.Method == "initiate" && len(output.Password) > 0 {

			passwordBytes := []byte(output.Password)
			passwordBytes = Crypto.SafeHash(passwordBytes)
			o.PasswordHash = passwordBytes
		}
		if t.Method == "participate" && len(output.Password) > 0 {
			pwd, err := hex.DecodeString(output.Password)
			if err != nil {
				panic(err)
			}
			o.PasswordHash = pwd
		}

		outputs = append(outputs, &o)
		glog.Infof("added output, %s of %d", output.Pubkey, output.Amount)
	}

	glog.Infof("creating transaction")
	transaction := (*apiHandler.Wallet).CreateTransaction(inputs, outputs)
	error := publishTransaction(transaction)
	if error == nil {
		outputChannle <- transaction
		return true
	}
	panic(error)
}
