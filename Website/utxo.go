package Website

import (
	"../Protobufs"
	"encoding/hex"
)

type utxo struct {
	Blockhash    string
	Txhash       string
	PasswordHash string
	OutputN      uint32
	Amount       uint64
	RevertTime   uint64
}

func newUtxo(q *Protobufs.UTXO) *utxo {
	toReturn := utxo{
		Blockhash:    hex.EncodeToString(q.Blockhash),
		Txhash:       hex.EncodeToString(q.Txhash),
		PasswordHash: hex.EncodeToString(q.PasswordHash),
		OutputN:      q.OutputN,
		Amount:       q.Amount,
		RevertTime:   q.RevertTime,
	}
	return &toReturn
}

func toUtxo(q *utxo) *Protobufs.UTXO {
	bh, err := hex.DecodeString(q.Blockhash)
	if err != nil {
		panic(err)
	}
	th, err := hex.DecodeString(q.Txhash)
	if err != nil {
		panic(err)
	}
	hp, err := hex.DecodeString(q.PasswordHash)
	if err != nil {
		panic(err)
	}
	toReturn := Protobufs.UTXO{
		Blockhash:    bh,
		Txhash:       th,
		PasswordHash: hp,
		OutputN:      q.OutputN,
		Amount:       q.Amount,
		RevertTime:   q.RevertTime,
	}
	return &toReturn
}
