package Website

import (
	"../Wallet"
	"encoding/json"
	"github.com/gogo/protobuf/proto"
	"github.com/golang/glog"
	"github.com/julienschmidt/httprouter"
	"net/http"
	"reflect"
)

var (
	apiHandler    *APIHandler
	outputChannle chan<- proto.Message
)

type inputdto struct {
	InputQuery querydto
	Password   string
}

type querydto struct {
	OutputN uint32
	Txhash  string
	Amount  uint64
}

type outputdto struct {
	RevertTime uint64
	Amount     uint64
	Pubkey     string
	Password   string
}

type inputoutputset struct {
	Inputs  []inputdto
	Outputs []outputdto
	Method  string
}

func apiHandlerFunction(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	action := ps[0].Value
	query := r.URL.Query()
	reflectParams := reflect.ValueOf(query)
	method := reflect.ValueOf(apiHandler).MethodByName(action + "Action")

	results := method.Call([]reflect.Value{reflectParams})
	returnType := results[0].Type()
	if returnType.Name() == "bool" {
		b := results[0].Bool()
		toReturn, err := json.Marshal(b)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(toReturn)
		return
	}
	value := results[0].Interface()
	toReturn, err := json.Marshal(value)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(toReturn)
}

func apiPostHandler(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	action := ps[0].Value
	err := r.ParseForm()
	if err != nil {
		panic(err)
	}
	form := r.Form
	glog.Info(action)

	method := reflect.ValueOf(apiHandler).MethodByName(action + "PostAction")

	reflectParams := reflect.ValueOf(form)
	reflectionBody := reflect.ValueOf(r.Body)
	results := method.Call([]reflect.Value{reflectParams, reflectionBody})
	returnType := results[0].Type()
	if returnType.Name() == "bool" {
		b := results[0].Bool()
		toReturn, err := json.Marshal(b)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(toReturn)
		return
	}
	value := results[0].Interface()
	toReturn, err := json.Marshal(value)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(toReturn)
}

// StartWebsite initializes a new instance of the website.
func StartWebsite(qwallet **Wallet.Wallet, port string, outChan chan<- proto.Message) {
	apiHandler = &APIHandler{Wallet: qwallet}
	// wallet = qwallet
	outputChannle = outChan
	router := httprouter.New()
	router.GET("/api/:action", apiHandlerFunction)
	router.POST("/api/:action", apiPostHandler)
	router.GET("/site/*filename", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		filename := ps[0].Value
		if filename == "" {
			filename = "index.html"
		}
		glog.Info(filename)
		http.ServeFile(w, r, "resources/"+filename)
	})

	go func() {
		glog.Fatal(http.ListenAndServe(":"+port, router))
	}()
}
