function getBlockHeight() {
    return $.getJSON("/api/GetLatestBlock");
}

function generateMnemonic(){
    return $.getJSON("/api/CreateMnemonic");
}

function login(mnemonic, password){
    return $.ajax({
        url:"/api/Login",
        method:"POST",
        data:{
            mnemonic: mnemonic,
            password:password
        }
    });
}

function startMiner(){
    return $.ajax({
        url:"/api/StartMiner",
        method:"POST"
    });
}

function stopMiner(){
    return $.ajax({
        url:"/api/StopMiner",
        method:"POST"
    });
}

function minerStatus(){
    return $.getJSON({
        url:"/api/MinerRunning",
        method:"GET"
    });
}

async function getUnspentOutputs(){
    var outputs = await $.getJSON("/api/GetUtxos");
    outputs = outputs.map(async (o)=>{
        let tx = await getTransaction(o.Txhash);
        if(o.OutputN == undefined){
            o.OutputN = 0;
        }
        return {output:o, transaction:tx};
    });
    outputs = await Promise.all(outputs);
    return outputs;
}

async function getTransaction(hash){
    return $.getJSON("/api/GetTransaction?hash=" + hash );
}

async function getBlock(hash){
    return $.getJSON("/api/GetBlock?hash=" + hash );
}

async function getBlocks(page){
    return $.getJSON("/api/GetBlocks?page=" + page);
}

function getHistory(){
    return $.getJSON("/api/GetHistory");
}

async function getBestSetUtxo(amount, pubkey){
    let latestBlock = await getBlockHeight();
    let latestBlockIndex = latestBlock.BlockNumber;
    let utxos = await getUnspentOutputs();
    utxos = utxos.sort(function(a, b){
        return a.output.Amount > b.output.Amount;
    });
    toReturn = [];
    for(var i = 0; i<utxos.length; i++){
        var utxo = utxos[i];
        console.log(utxo);
        if(utxo.output.PasswordHash != ""){
            continue;
        }
        var output = utxo.transaction.Outputs[utxo.output.OutputN];
        if(utxo.output.PasswordHash == "" && output.Pubkey != pubkey){
            console.log(utxo, pubkey);

            continue;
        }
        var block = await getBlock(utxo.output.Blockhash);
        console.log(block.blockNumber);
        console.log(utxo.output.RevertTime);
        console.log(latestBlockIndex);
        if(block.BlockNumber + utxo.output.RevertTime >= latestBlockIndex){
            continue;
        }
        toReturn.push(utxos[i].output);
        if(toReturn.reduce((current, accumulator)=>{
            return current + accumulator.Amount;
        }, 0) > amount ){
            return toReturn;
        }
    }
    return [];
}

function sendTransaction(inputs, outputs, method){
    return $.ajax({
        url:"/api/SendTransaction",
        method:"POST",
        data: JSON.stringify({Inputs:inputs, Outputs:outputs, Method:method}),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json'
    });
}

function sendRevertTransaction(query){
    return $.ajax({
        url:"/api/RevertLockTransaction",
        method:"POST",
        data:JSON.stringify({InputQuery:query}),
        contentType: 'application/json; charset=utf-8',
        dataType:'json'
    });
}

function sendUnlockTransaction(query, password){
    return $.ajax({
        url:"/api/SendUnlockTransaction",
        method:"POST",
        data:JSON.stringify({InputQuery:query, Password:password}),
        contentType: 'application/json; charset=utf-8',
        dataType:'json'
    });
}

async function getBalance(){
    utxos = await getUnspentOutputs();
    toReturn = 0;
    console.log(utxos);
    utxos.forEach((utxo)=>{
        if(utxo.output.PasswordHash == ""){
            toReturn += utxo.output.Amount;
        }
    });
    return toReturn;
}
