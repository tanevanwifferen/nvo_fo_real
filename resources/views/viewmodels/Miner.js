define(['knockout', "Scripts/data"], function(ko, data) {
    function myViewModelFactory(params, componentInfo) {
        console.log(params);
        var vm= {
            mineractive:ko.observable(false),
            startMiner:async function(){
                await startMiner();
                vm.mineractive(await minerStatus());
            },
            stopMiner:async function(){
                await stopMiner();
                vm.mineractive(await minerStatus());
            }
        };
        var a = async function(){
            vm.mineractive(await minerStatus());
        };
        a();

        return vm;
    }
    return { createViewModel: myViewModelFactory };
});
