define(["knockout", "Scripts/data"], function(ko) {
    function myViewModelFactory(params, componentInfo) {

        var viewmodel = {
            key: params.key,
            swaps: ko.observableArray([]),
        };

        viewmodel.Unlock = function(item, event, index){
            console.log("index", index);
            console.log("item:", item);
            console.log(item);
            var query = {
                TransactionHash: item.utxo.output.Txhash,
                OutputNumber: item.utxo.output.OutputN
            };

            var password = $("#releasePassword" + index).val();
            sendUnlockTransaction(query, password);
        };

        viewmodel.revertTransaction = function(item, event, index){
            console.log("index", index);
            console.log("item:", item);
            console.log(item);
            var query = {
                TransactionHash: item.utxo.output.Txhash,
                OutputNumber: item.utxo.output.OutputN
            };
            sendRevertTransaction(query);
        };

        viewmodel.swaps.subscribe((newValue)=>{
            console.log(newValue);
        });
        async function updateSwaps(){
            let utxos = await getUnspentOutputs();
            console.log(utxos);
            let swaps = utxos.filter(x => x.output.PasswordHash != "")
                .map(async (utxo) => {
                    var block = await getBlock(utxo.output.Blockhash);
                    return {utxo: utxo, block: block};
                });
            Promise.all(swaps).then(completed => viewmodel.swaps(completed));
        }
        updateSwaps();

        return viewmodel;
    }

    return { createViewModel: myViewModelFactory };
});
