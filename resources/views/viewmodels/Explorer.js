// AMD module whose value is a component viewmodel constructor
define(["knockout", "crossroads", "Scripts/data"], function(ko, crossroads) {
    function myViewModelFactory(params, componentInfo) {
        var viewmodel = {
            page: params.page,
            target: ko.observable(null),
            blocks:ko.observableArray([]),
            goBack: function(){
                crossroads.parse("/site/explorer/" + (Number(params.page()) - 1));
            },
            goForward: function(){
                crossroads.parse("/site/explorer/" + (Number(params.page()) + 1));
            },
        };

        viewmodel.showBlockDetails = function(block){
            viewmodel.target(block);
            console.log("showblockdetails called");
            $('#blockdetails').modal();
        };

        viewmodel.closeBlockDetails = function(){
            viewmodel.target(null);
            $('#blockdetails').modal('hide');
        };

        var load = async function(){
            var blocks = await getBlocks(params.page());
            console.log(blocks);
            viewmodel.blocks(blocks);
        };
        params.page.subscribe(async function(newValue){
            var blocks = await getBlocks(newValue);
            console.log(blocks);
            viewmodel.blocks(blocks);
        });
        load();
        return viewmodel;
    }

    return { createViewModel: myViewModelFactory };
});
