// AMD module whose value is a component viewmodel constructor
define(["knockout", "crossroads", "Scripts/data"], function(ko, crossroads) {
    function myViewModelFactory(params, componentInfo) {
        var intervalId = 0;
        var viewmodel = {
            openWallet: function(){
                //params.window("wallet")
                console.log("open wallet clicked");
                crossroads.parse('/site/wallet');
            },
            openExplorer: function(){
                //params.window("explorer")
                console.log("open explorer clicked");
                crossroads.parse('/site/explorer');
            },
            openMiner: function(){
                //params.window("miner")
                console.log("open miner clicked");
                crossroads.parse('/site/miner');
            },
            openSwaps: function(){
                //params.window("miner")
                console.log("open miner clicked");
                crossroads.parse('/site/swaps');
            },
            latestBlock : ko.observable(null),
            activePage: params.window,
            dispose: function(){
                clearInterval(intervalId);
            }
        };
        async function updateHeight(){
            var data = await getBlockHeight();
            viewmodel.latestBlock(data);
        }
        intervalId = setInterval(updateHeight, 10000);
        updateHeight();
        return viewmodel;
    }

    return { createViewModel: myViewModelFactory };
});
