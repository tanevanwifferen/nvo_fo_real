var vm_internal = {};

async function initiateSwap(){
    let pubkey = $("#initiate_pubkey").val();
    let amount = Number($("#initiate_amount").val());
    let fees = Number($("#initiate_fees").val());
    let password = $("#initiate_password").val();
    let revertTime = Number($("#initiate_revert_time").val());
    var inputs = await getBestSetUtxo(amount, vm_internal.loggedInPubkey());
    if(inputs.length == 0){
        return;
    }
    inputs = inputs.map((input)=>{
        return {InputQuery:input, password:""};
    });

    var outputs = [];
    outputs.push({
        Amount:amount,
        Pubkey:pubkey,
        Password:password,
        RevertTime:revertTime
    });

    var totalInputs = inputs.reduce((cur, acc)=>{
        return cur + acc.InputQuery.Amount;
    }, 0);
    if(totalInputs - amount - fees > 0){
        outputs.push({
            RevertTime:0,
            Amount:totalInputs - amount - fees,
            Pubkey:vm_internal.loggedInPubkey(),
            Password:""
        });
    }

    sendTransaction(inputs, outputs, "initiate");
}

async function participateSwap(){
    let pubkey = $("#participate_pubkey").val();
    let amount = Number($("#participate_amount").val());
    let fees = Number($("#participate_fees").val());
    let password = $("#participate_password_hash").val();
    let revertTime = Number($("#participate_revert_time").val());
    var inputs = await getBestSetUtxo(amount, vm_internal.loggedInPubkey());
    if(inputs.length == 0){
        return;
    }
    inputs = inputs.map((input)=>{
        return {InputQuery:input, password:""};
    });

    var outputs = [];
    outputs.push({
        Amount:amount,
        Pubkey:pubkey,
        Password:password,
        RevertTime:revertTime
    });

    var totalInputs = inputs.reduce((cur, acc)=>{
        return cur + acc.InputQuery.Amount;
    }, 0);
    if(totalInputs - amount - fees > 0){
        outputs.push({
            RevertTime:0,
            Amount:totalInputs - amount - fees,
            Pubkey:vm_internal.loggedInPubkey(),
            Password:""
        });
    }

    sendTransaction(inputs, outputs, "participate");
}

async function sendCoins(){
    let pubkey = $("#send_pubkey").val();
    let amount = Number($("#send_amount").val());
    let fees = Number($("#send_fees").val());
    var inputs = await getBestSetUtxo(amount, vm_internal.loggedInPubkey());
    if(inputs.length == 0){
        console.log("no inputs fit size");
        return;
    }
    inputs = inputs.map((input)=>{
        return {InputQuery:input, password:""};
    });

    var outputs = [];
    outputs.push({
        Amount:amount,
        Pubkey:pubkey
    });

    var totalInputs = inputs.reduce((cur, acc)=>{
        return cur + acc.InputQuery.Amount;
    }, 0);
    if(totalInputs - amount - fees > 0){
        outputs.push({
            RevertTime:0,
            Amount:totalInputs - amount - fees,
            Pubkey:vm_internal.loggedInPubkey(),
            Password:""
        });
    }


    sendTransaction(inputs, outputs, "send");
}

define(['knockout', "Scripts/data"], function(ko) {
    function myViewModelFactory(params, componentInfo) {
        console.log(params);
        var intervalTimer = 0;
        vm_internal = {
            loggedInPubkey: params.key,
            sendCoins: sendCoins,
            transactions: ko.observableArray(),
            balance:ko.observable(0),
            dispose: function(){
                clearInterval(intervalTimer);
            }
        };
        var b = async function(){
            console.log("b called");
            var valance = await getBalance();
            vm_internal.balance(valance);
            vm_internal.transactions(await getHistory());
        };
        intervalTimer = setInterval(b, 15000);
        b();

        return vm_internal;
    }
    return { createViewModel: myViewModelFactory };
});
