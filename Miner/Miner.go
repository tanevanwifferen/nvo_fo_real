package Miner

import (
	"../Blockchain"
	"../Mempool"
	"../Protobufs"
	"../Util"
	"../Wallet"
	"bytes"
	"encoding/binary"
	"encoding/hex"
	"errors"
	"github.com/golang/glog"
	"time"
)

func intToByteArray(in uint64) []byte {
	buff := new(bytes.Buffer)
	err := binary.Write(buff, binary.BigEndian, in)
	if err != nil {
		glog.Fatal(err)
	}

	intByteArray := buff.Bytes()
	return intByteArray
}

// MineRootBlock mines the first block in the chain
func MineRootBlock(
	wallet *Wallet.Wallet,
	difficulty uint64,
) *Protobufs.ProofOfWorkBlock {
	pubkey := wallet.GetPubKey()
	rootBlock := Protobufs.ProofOfWorkBlock{
		BlockNumber:   0,
		Difficulty:    difficulty,
		PubKey:        pubkey,
		PreviousBlock: make([]byte, 0),
	}
	now := time.Now()
	secs := now.Unix()
	if secs < 0 {
		panic("utc < 0")
	}
	rootBlock.Timestamp = uint64(secs)
	tx := Protobufs.Transaction{
		Pubkey: pubkey,
	}
	output := Protobufs.Output{
		Pubkey:     pubkey,
		Amount:     1000000000,
		RevertTime: 10,
	}
	tx.Outputs = append(tx.Outputs, &output)
	txHash, _ := Blockchain.HashTransaction(&tx)
	tx.Hash = txHash
	tx.Signature = wallet.SignHash(txHash)
	rootBlock.Transactions = append(rootBlock.Transactions, &tx)

	var nonce uint64
	target := Blockchain.DifficultyToMaxHashSize(rootBlock.Difficulty)
	for {
		nonceBytes := intToByteArray(nonce)
		rootBlock.Nonce = nonceBytes
		hash := Util.HashBlock(&rootBlock)
		if Blockchain.HashSmallerThanTarget(hash, target) {
			rootBlock.Hash = hash
			signature := wallet.SignHash(hash)
			rootBlock.Signature = signature

			glog.Info("nonce:" + hex.EncodeToString(rootBlock.Nonce))
			glog.Info("hash:" + hex.EncodeToString(rootBlock.Hash))
			glog.Info("sig:" + hex.EncodeToString(rootBlock.Signature))
			glog.Info("pub:" + hex.EncodeToString(rootBlock.PubKey))
			return &rootBlock
		}
		nonce = nonce + 1
	}
}

func createCoinbaseTransaction(
	amount uint64,
	wallet *Wallet.Wallet,
	blockNumber uint64,
) *Protobufs.Transaction {
	pubkey := wallet.GetPubKey()
	tx := Protobufs.Transaction{
		Pubkey: pubkey,
		Data:   []byte(string(blockNumber)),
	}
	now := time.Now()
	secs := now.Unix()
	if secs < 0 {
		glog.Fatal("utc < 0")
	}
	tx.Timestamp = uint64(secs)
	output := Protobufs.Output{
		Pubkey:     pubkey,
		Amount:     amount,
		RevertTime: 10,
	}
	tx.Outputs = append(tx.Outputs, &output)
	txHash, _ := Blockchain.HashTransaction(&tx)
	tx.Hash = txHash
	tx.Signature = wallet.SignHash(txHash)
	return &tx
}

func mineBlock(
	block *Protobufs.ProofOfWorkBlock,
	cancellationToken chan bool,
	blocksToReturn chan *Protobufs.ProofOfWorkBlock,
	wallet *Wallet.Wallet,
) {

	newDifficulty := Blockchain.CalculateNextDifficulty(block)
	glog.Infof("Miner new difficulty: %d", newDifficulty)
	target := Blockchain.DifficultyToMaxHashSize(newDifficulty)

	newBlock := Protobufs.ProofOfWorkBlock{
		BlockNumber:   block.BlockNumber + 1,
		Difficulty:    newDifficulty,
		PubKey:        wallet.GetPubKey(),
		PreviousBlock: block.Hash,
	}

	wrappers := Mempool.GetForBlock(200)
	transactions := make([]*Protobufs.Transaction, 0)
	feesPaid := uint64(0)
	for _, tx := range wrappers {
		feesPaid = feesPaid + tx.Fees
		transactions = append(transactions, tx.Tx)
	}

	if len(transactions) > 0 {
		minerFee := feesPaid / 2
		coinbase := createCoinbaseTransaction(minerFee, wallet, newBlock.BlockNumber)
		newBlock.Transactions = append(newBlock.Transactions, coinbase)
		newBlock.Transactions = append(newBlock.Transactions, transactions...)
	}

	var nonce uint64
	for {
		select {
		case <-cancellationToken:
			// stop
			glog.Info("cancelling miner")
			return
		default:
			nonceBytes := intToByteArray(nonce)
			newBlock.Nonce = nonceBytes
			hash := Util.HashBlock(&newBlock)
			if Blockchain.HashSmallerThanTarget(hash, target) {
				now := time.Now()
				secs := now.Unix()
				if secs < 0 {
					panic("utc < 0")
				}
				newBlock.Timestamp = uint64(secs)
				newBlock.Hash = hash
				signature := wallet.SignHash(hash)
				newBlock.Signature = signature
				glog.Info("miner: found new block")
				blocksToReturn <- &newBlock
				for _, tx := range newBlock.Transactions {
					Mempool.RemoveTx(tx.Hash, true)
				}
				glog.Info("miner: sent new block away")
				return
			}
			nonce = nonce + 1
		}
	}
}

func miningProcess(
	newBlocks chan *Protobufs.ProofOfWorkBlock,
	blocksToReturn chan *Protobufs.ProofOfWorkBlock,
	wallet *Wallet.Wallet,
) {
	cancellationToken := make(chan bool)
	running := false
	for block := range newBlocks {
		latestBlock, err := Blockchain.GetLatestBlock()
		if err == nil {
			if !bytes.Equal(latestBlock.Hash, block.Hash) {
				continue
			}
		}
		if running {
			glog.Info("closing old miner")
			close(cancellationToken)
			running = false
			cancellationToken = make(chan bool)
			glog.Info("close completed")
		}
		running = true
		go mineBlock(block, cancellationToken, blocksToReturn, wallet)
	}
	close(cancellationToken)
}

// GetMiner returns a miner instance
func getMiner(
	wallet *Wallet.Wallet,
	newBlocks chan *Protobufs.ProofOfWorkBlock,
) <-chan *Protobufs.ProofOfWorkBlock {
	outChan := make(chan *Protobufs.ProofOfWorkBlock)
	go miningProcess(newBlocks, outChan, wallet)
	return outChan
}

var channleID = 0

// StartMiner starts the miner
func StartMiner(wallet *Wallet.Wallet) (e error) {
	if channleID != 0 {
		return errors.New("already mining")
	}
	minerInStream := make(chan *Protobufs.ProofOfWorkBlock)
	minerOutStream := getMiner(wallet, minerInStream)
	go func() {
		for block := range minerOutStream {
			Blockchain.InsertBlock(block)
		}
	}()
	channleID = Blockchain.ListenToBlockUpdates(minerInStream)
	return nil
}

// StopMiner stops the miner
func StopMiner() {
	if channleID != 0 {
		Blockchain.UnListenToBlockUpdates(channleID)
		channleID = 0
	}
}

// MinerRunning returns a value indicating whether the miner is running
func MinerRunning() bool {
	return channleID != 0
}
