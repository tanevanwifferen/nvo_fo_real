package Mempool

import (
	"../Blockchain"
	"../Protobufs"
	"bytes"
	"encoding/hex"
	"errors"
	"github.com/golang/glog"
	"sort"
	"sync"
)

var (
	transactions = make([]TxWrapper, 0)
	mutex        = &sync.Mutex{}
)

// TxWrapper is a wrapper that enables the mempool to sort by fees
type TxWrapper struct {
	Tx   *Protobufs.Transaction
	Fees uint64
}

// AddTx adds transaction to mempool.
func AddTx(tx *Protobufs.Transaction) error {
	valid, fees := Blockchain.VerifyTransaction(tx, false)
	if !valid {
		return errors.New("block invalid")
	}

	mutex.Lock()
	defer mutex.Unlock()

	for _, el := range transactions {
		if bytes.Equal(el.Tx.Hash, tx.Hash) {
			return errors.New("already in mempool")
		}
		for _, inputa := range el.Tx.Inputs {
			queryA := inputa.InputQuery
			for _, inputb := range tx.Inputs {
				queryB := inputb.InputQuery
				if bytes.Equal(queryA.TransactionHash, queryB.TransactionHash) {
					if queryA.OutputNumber == queryB.OutputNumber {
						RemoveTx(el.Tx.Hash, false)
						return errors.New("double spend")
					}
				}
			}
		}
	}

	transactions = append(transactions, TxWrapper{
		Tx:   tx,
		Fees: fees,
	})
	glog.Infof("adding transaction: %s, with %d fees paid", hex.EncodeToString(tx.Hash), fees)
	sort.Slice(transactions, func(a, b int) bool {
		return transactions[a].Fees > transactions[b].Fees
	})
	for i := 0; i < len(transactions); i++ {
		glog.Info(transactions[i].Fees)
	}
	TrimMemPool(5000)
	return nil
}

// TrimMemPool removes the cheapest transactions from the mempool when
// memory is filling up.
func TrimMemPool(limit uint) {
	if int(limit) < len(transactions) {
		mutex.Lock()
		defer mutex.Unlock()
		transactions = transactions[:limit]
	}
}

// RemoveTx removes a transaction by hash from the mempool
func RemoveTx(hash []byte, shouldLock bool) {
	if shouldLock {
		mutex.Lock()
		defer mutex.Unlock()
	}
	for index, el := range transactions {
		if bytes.Equal(hash, el.Tx.Hash) {
			transactions = append(transactions[0:index], transactions[index+1:]...)
			return
		}
	}
}

// GetForBlock gets a set of transactions for a miner
func GetForBlock(limit int) []TxWrapper {
	if len(transactions) == 0 {
		return transactions
	}
	if limit < len(transactions) {
		return transactions[:limit]
	}
	return transactions
}
