package Wallet

import (
	"../Blockchain"
	"../Crypto"
	"../Protobufs"

	"bytes"
	"encoding/hex"
	"errors"
	"time"

	"github.com/golang/glog"
)

const (
	path = "./local/"
)

// Wallet struct holds keys and signing functionality
type Wallet struct {
	pub, priv []byte
}

// NewWallet initializes a new wallet instance
func NewWallet(mnemonic string, password string) (wallet *Wallet, e error) {
	seed := Crypto.GetSeed(mnemonic, password)
	pub, priv := Crypto.GetKeyPair(seed)
	glog.Infof("Your seed= %s", hex.EncodeToString(seed))
	glog.Infof("Your public key= %s", hex.EncodeToString(pub))
	glog.Infof("Your private key= %s", hex.EncodeToString(priv))
	return &Wallet{pub: pub, priv: priv}, nil
}

// SignHash signs a hash for the logged in user
func (state *Wallet) SignHash(hash []byte) []byte {
	return Crypto.SafeSign(hash, state.priv)
}

// GetPubKey returns the public key of the wallet
func (state *Wallet) GetPubKey() []byte {
	return state.pub
}

// CreateTransaction creates a transaction.
func (state *Wallet) CreateTransaction(inputs []*Protobufs.Input, outputs []*Protobufs.Output) *Protobufs.Transaction {
	now := time.Now()
	secs := now.Unix()
	if secs < 0 {
		glog.Fatal("utc < 0")
	}

	tx := Protobufs.Transaction{
		Pubkey:    state.pub,
		Inputs:    inputs,
		Outputs:   outputs,
		Timestamp: uint64(secs),
	}
	// _ is sighash. Is not used as no signature is set yet
	txHash, _ := Blockchain.HashTransaction(&tx)
	tx.Hash = txHash
	tx.Signature = state.SignHash(txHash)
	return &tx
}

// FetchTransactions fetches all incoming and outgoing transactions for the
// logged in user
func (state *Wallet) FetchTransactions() ([]*Protobufs.Transaction, error) {
	toReturn := make([]*Protobufs.Transaction, 0)
	latestBlock, err := Blockchain.GetLatestBlock()
	if err != nil {
		return toReturn, err
	}
	for {
		for _, tx := range latestBlock.Transactions {
			if bytes.Equal(tx.Pubkey, state.pub) {
				toReturn = append(toReturn, tx)
				continue
			}
			for _, o := range tx.Outputs {
				if bytes.Equal(o.Pubkey, state.pub) {
					toReturn = append(toReturn, tx)
				}
			}
		}
		latestBlock, err = Blockchain.GetPreviousBlock(latestBlock)
		if err != nil {
			return toReturn, nil
		}
	}
}

// CreateUnlockTransaction returns an unlock transaction
func (state *Wallet) CreateUnlockTransaction(
	input *Protobufs.Input,
) (*Protobufs.Transaction, error) {
	inputs := make([]*Protobufs.Input, 0)
	inputs = append(inputs, &Protobufs.Input{
		InputQuery: input.InputQuery,
		Password:   input.Password,
	})

	oldOutput, err := Blockchain.GetUTXO(input.InputQuery)
	if err != nil {
		panic(err)
	}

	pwdHash := Crypto.SafeHash(input.Password)

	if !bytes.Equal(oldOutput.PasswordHash, pwdHash) {
		glog.Info("password doesn't match hash")
		return nil, errors.New("password doesn't match hash")
	}

	amount := oldOutput.Amount

	outputs := make([]*Protobufs.Output, 0)
	outputs = append(outputs, &Protobufs.Output{
		Pubkey: state.pub,
		Amount: amount,
	})
	return state.CreateTransaction(inputs, outputs), nil
}

func getUTXOsForBlock(block *Protobufs.ProofOfWorkBlock, pubkey []byte) []*Protobufs.UTXO {
	toReturn := make([]*Protobufs.UTXO, 0)
	for _, tx := range block.Transactions {
		for index, output := range tx.Outputs {
			// add transaction when it is an plain transaction to me,
			// or a lock to me
			// or a transaction that can be reverted
			if bytes.Equal(output.Pubkey, pubkey) {
				query := Protobufs.Query{
					TransactionHash: tx.Hash,
					OutputNumber:    uint32(index),
				}
				utxo, err := Blockchain.GetUTXO(&query)
				if err == nil {
					toReturn = append(toReturn, utxo)
				}
				continue
			}
			if bytes.Equal(tx.Pubkey, pubkey) && output.RevertTime > 0 && len(output.PasswordHash) > 0 {
				query := Protobufs.Query{
					TransactionHash: tx.Hash,
					OutputNumber:    uint32(index),
				}
				utxo, err := Blockchain.GetUTXO(&query)
				if err == nil {
					toReturn = append(toReturn, utxo)
				}
				continue
			}

		}
	}
	return toReturn
}

// GetUTXOs returns the utxos for a given pubkey
func (state *Wallet) GetUTXOs() []*Protobufs.UTXO {
	latestBlock, err := Blockchain.GetLatestBlock()
	if err != nil {
		glog.Info(err)
		return make([]*Protobufs.UTXO, 0)
	}
	toReturn := make([]*Protobufs.UTXO, 0)
	toReturn = append(toReturn, getUTXOsForBlock(latestBlock, state.pub)...)
	block := latestBlock
	for {
		block, err = Blockchain.GetPreviousBlock(block)
		if err != nil {
			return toReturn
		}
		toReturn = append(toReturn, getUTXOsForBlock(block, state.pub)...)
	}
	glog.Info(err)
	return toReturn
}
