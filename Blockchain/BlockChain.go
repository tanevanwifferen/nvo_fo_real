package Blockchain

import (
	"../Crypto"
	"../Protobufs"
	"../Storage"
	"bytes"
	"encoding/binary"
	"encoding/hex"
	"errors"
	"github.com/golang/glog"
	"github.com/perlin-network/noise/network"
	"math/big"
	"sync"
)

var (
	leafNodeKey       = []byte("LeafBlocks")
	newBlockListeners = make([]channlewrapper, 0)
)

type channlewrapper struct {
	index int
	block chan<- *Protobufs.ProofOfWorkBlock
}

// ListenToBlockUpdates subscribes a channel to new block updates
func ListenToBlockUpdates(inChan chan<- *Protobufs.ProofOfWorkBlock) int {
	i := len(newBlockListeners)
	wrapper := channlewrapper{
		index: i,
		block: inChan,
	}
	newBlockListeners = append(newBlockListeners, wrapper)
	latestBlock, err := CalcBestLeafNode()
	if err == nil {
		inChan <- latestBlock
	}
	return i
}

// UnListenToBlockUpdates unsubscribes the given channel
func UnListenToBlockUpdates(i int) {
	for index, wrapper := range newBlockListeners {
		if wrapper.index == i {
			newBlockListeners = append(newBlockListeners[:index], newBlockListeners[index+1:]...)
			close(wrapper.block)
		}
	}
}

func publishBlock(block *Protobufs.ProofOfWorkBlock) {
	for _, wrapper := range newBlockListeners {
		wrapper.block <- block
	}
}

// GetLeafNodes returns the leaf blocks from the blockchain
func GetLeafNodes() []*Protobufs.Leaf {
	var msg Protobufs.LeafWrapper
	_ = Storage.Read(leafNodeKey, &msg)
	return msg.Leafs
}

// GetBlock returns a block with a given hash
func GetBlock(hash []byte) (*Protobufs.ProofOfWorkBlock, error) {
	var block Protobufs.ProofOfWorkBlock
	err := Storage.Read(hash, &block)
	return &block, err
}

// CalculateTotalDifficulty recursively goes back through the chain to calculate the
// total difficulty until given block
func CalculateTotalDifficulty(block *Protobufs.ProofOfWorkBlock) uint64 {
	glog.Infof("Calculating difficulty for block %s", hex.EncodeToString(block.Hash))
	toReturn := block.Difficulty
	newBlock := block
	var err error
	for {
		newBlock, err = GetPreviousBlock(newBlock)
		if err != nil {
			glog.Infof("total difficulty: %d", toReturn)
			return toReturn
		}
		toReturn = toReturn + newBlock.Difficulty
	}
}

// GetTransaction attempts to fetch a transaction from the database
func GetTransaction(hash []byte) (tx *Protobufs.Transaction, blockID []byte, e error) {
	knownBlocks := make(map[string]bool)
	leafs := GetLeafNodes()
	for _, leaf := range leafs {
		blockID := leaf.BlockHash

		for {
			stringBlockID := string(blockID)
			if knownBlocks[stringBlockID] {
				continue
			}
			block, err := GetBlock(blockID)
			if err != nil {
				return &Protobufs.Transaction{}, make([]byte, 0), err
			}
			for _, tx := range block.Transactions {
				if bytes.Equal(tx.Hash, hash) {
					return tx, block.Hash, nil
				}
			}
			knownBlocks[stringBlockID] = true
			blockID = block.PreviousBlock
		}

	}
	return &Protobufs.Transaction{}, make([]byte, 0), errors.New("tx not found")
}

// CalcBestLeafNode returns the node with the most consecutive difficulty
func CalcBestLeafNode() (bestBlock *Protobufs.ProofOfWorkBlock, e error) {
	leafNodes := GetLeafNodes()
	if len(leafNodes) == 0 {
		return &Protobufs.ProofOfWorkBlock{}, errors.New("No leaf nodes found")
	}
	if len(leafNodes) == 1 {
		return GetBlock(leafNodes[0].BlockHash)
		//return leafNodes[0], nil
	}

	max := uint64(0)
	var toReturn *Protobufs.Leaf
	for _, leaf := range leafNodes {
		if leaf.TotalDifficulty > max {
			toReturn = leaf
			max = leaf.TotalDifficulty
		}
	}
	glog.Infof("calcbestleafnode terminated, toReturn:%s", toReturn)
	return GetBlock(toReturn.BlockHash)
}

// GetLatestBlock returns the latest block in the blockchain
func GetLatestBlock() (returnBlock *Protobufs.ProofOfWorkBlock, error error) {
	return CalcBestLeafNode()
}

const noPreviousBlockFoundErr = "No previous block found"

// GetPreviousBlock returns the block that came before it
func GetPreviousBlock(block *Protobufs.ProofOfWorkBlock) (*Protobufs.ProofOfWorkBlock, error) {
	previous := block.PreviousBlock
	empty := Protobufs.ProofOfWorkBlock{}
	if previous == nil {
		return &empty, errors.New(noPreviousBlockFoundErr)
	}
	var msg Protobufs.ProofOfWorkBlock
	err := Storage.Read(previous, &msg)
	if err != nil {
		return &empty, errors.New(noPreviousBlockFoundErr)
	}
	return &msg, nil
}

var mutex = &sync.Mutex{}

func updateLeafNode(block *Protobufs.ProofOfWorkBlock) {
	glog.Infof("updating leaf nodes")
	leafs := GetLeafNodes()
	if block.BlockNumber == 0 || len(leafs) == 0 {
		leaf := Protobufs.Leaf{
			BlockHash:       block.Hash,
			BlockNumber:     block.BlockNumber,
			TotalDifficulty: block.Difficulty,
		}
		leafs = append(leafs, &leaf)
	} else {
		glog.Infof("Comparing with other leaf nodes")
		for index, leaf := range leafs {
			currHash := &big.Int{}
			currHash = currHash.SetBytes(block.PreviousBlock)

			prevHash := &big.Int{}
			prevHash = prevHash.SetBytes(leaf.BlockHash)
			if prevHash.Cmp(currHash) == 0 {
				glog.Infof("Updating old leaf")
				leafs[index] = &Protobufs.Leaf{
					BlockHash:       block.Hash,
					BlockNumber:     block.BlockNumber,
					TotalDifficulty: leaf.TotalDifficulty + block.Difficulty,
				}
				Storage.Set(leafNodeKey, &Protobufs.LeafWrapper{Leafs: leafs})
				glog.Infof("updated leafnodes, new leaf nodes are %s", leafs)
				return
			}
		}

		glog.Infof("Setting new leaf")
		newleaf := Protobufs.Leaf{
			BlockNumber:     block.BlockNumber,
			BlockHash:       block.Hash,
			TotalDifficulty: CalculateTotalDifficulty(block),
		}
		leafs = append(leafs, &newleaf)
	}
	glog.Infof("Setting new leafs in database")
	Storage.Set(leafNodeKey, &Protobufs.LeafWrapper{Leafs: leafs})
	glog.Infof("updated leafnodes, new leaf nodes are %s", leafs)
}

var orphanPrefix = []byte("orphan_")

func hashesAreEqual(a []byte, b []byte) bool {
	aInt := &big.Int{}
	aInt = aInt.SetBytes(a)

	bInt := &big.Int{}
	bInt = bInt.SetBytes(b)
	return aInt.Cmp(bInt) == 0
}

func insertOrphanBlock(block *Protobufs.ProofOfWorkBlock) error {
	// set as orphan block
	glog.Infof("block %s is orphan", hex.EncodeToString(block.Hash))
	b := Protobufs.ProofOfWorkBlockWrapper{}
	key := append(orphanPrefix, block.PreviousBlock...)
	_ = Storage.Read(key, &b)
	for _, orphanBlock := range b.Blocks {
		if hashesAreEqual(orphanBlock.Hash, block.Hash) {
			return nil
		}
	}
	b.Blocks = append(b.Blocks, block)
	Storage.Set(key, &b)
	return nil
}

func insertNonOrphanBlock(block *Protobufs.ProofOfWorkBlock) error {
	// set as non orphan block
	// update leaf nodes
	// update all orphan blocks
	if !VerifyBlock(block) {
		glog.Infof("block is %s not valid", hex.EncodeToString(block.Hash))
		return errors.New("block invalid")
	}
	glog.Infof("block %s is not orphan", hex.EncodeToString(block.Hash))
	Storage.Set(block.Hash, block)
	glog.Infof("block has %d transactions", len(block.Transactions))
	for _, tx := range block.Transactions {
		updateUTXO(tx, block)
	}
	glog.Infof("inserted block %s", hex.EncodeToString(block.Hash))
	updateLeafNode(block)

	// update orphans
	b := Protobufs.ProofOfWorkBlockWrapper{}
	key := append(orphanPrefix, block.Hash...)
	_ = Storage.Read(key, &b)

	if len(b.Blocks) > 0 {
		for _, orphan := range b.Blocks {
			glog.Infof("entering orphan loop")
			if VerifyBlock(orphan) {
				err := insertNonOrphanBlock(orphan)
				if err != nil {
					return err
				}
			}
			orphansBeingChecked[hex.EncodeToString(orphan.PreviousBlock)] = false
		}
		Storage.Delete(key)
	}
	return nil
}

// TODO: move to util
func hashQuery(q *Protobufs.Query) []byte {
	b := make([]byte, 4)
	binary.LittleEndian.PutUint32(b, q.OutputNumber)
	toHash := append(q.TransactionHash, b...)
	return Crypto.SafeHash(toHash)
}

// GetUTXO gets an unspent transaction by query. returns error when not found
func GetUTXO(q *Protobufs.Query) (*Protobufs.UTXO, error) {
	hash := hashQuery(q)
	utxo := Protobufs.UTXO{}
	err := Storage.Read(hash, &utxo)
	return &utxo, err
}

func updateUTXO(tx *Protobufs.Transaction, block *Protobufs.ProofOfWorkBlock) {
	glog.Info("updating utxo")
	for _, input := range tx.Inputs {
		glog.Info("deleting inputs")
		queryHash := hashQuery(input.InputQuery)
		Storage.Delete(queryHash)
	}
	for index, output := range tx.Outputs {
		glog.Info("adding outputs")
		query := Protobufs.Query{
			TransactionHash: tx.Hash,
			OutputNumber:    uint32(index),
		}
		queryHash := hashQuery(&query)
		utxo := Protobufs.UTXO{
			Blockhash:    block.Hash,
			Txhash:       tx.Hash,
			PasswordHash: output.PasswordHash,
			OutputN:      uint32(index),
			Amount:       output.Amount,
			RevertTime:   output.RevertTime,
		}
		Storage.Set(queryHash, &utxo)
	}
}

// InsertBlock adds a block to the chain
func InsertBlock(block *Protobufs.ProofOfWorkBlock) error {
	mutex.Lock()
	defer mutex.Unlock()
	// check if block is orphan
	_, err := GetPreviousBlock(block)
	if err != nil && err.Error() == noPreviousBlockFoundErr && block.BlockNumber != 0 {
		err = insertOrphanBlock(block)
	} else {
		err = insertNonOrphanBlock(block)
		if err == nil {
			publishBlock(block)
		}
	}

	return err
}

var orphansBeingChecked = make(map[string]bool)

// CheckOrphanStatus is a thorn in my eye. TODO: please fix this
func CheckOrphanStatus(ctx *network.PluginContext, orphan *Protobufs.ProofOfWorkBlock) {
	_, err := GetPreviousBlock(orphan)
	glog.Infof("getting orphan status for %s", hex.EncodeToString(orphan.Hash))
	if err == nil {
		return
	}
	glog.Info("checking root block edge case")
	if len(orphan.PreviousBlock) == 0 {
		return
	}
	if !orphansBeingChecked[hex.EncodeToString(orphan.PreviousBlock)] {
		orphansBeingChecked[hex.EncodeToString(orphan.PreviousBlock)] = true
		glog.Info("sending request")
		msg := Protobufs.StringWrapper{Message: "getblock" + hex.EncodeToString(orphan.PreviousBlock)}
		glog.Infof("request= %s", msg)
		err := ctx.Client().Tell(&msg)
		if err != nil {
			glog.Fatal(err)
		}
	}
}
