package Blockchain

/*
Difficulty is calculated by looking at how much time has passed
since the creation of a block 6 blocks ago. This is supposed
to be 30 minutes, with 5 minutes per block. New difficulty is then
calculated as

oldDifficulty * (expectedTime / actualTime)
*/

import (
	"../Protobufs"
	"encoding/hex"
	"github.com/golang/glog"
	"math/big"
)

const (
	expectedDelta = 10 * 6 // 300 seconds expected time. Difficulty is ca
	// InitDifficulty is the starting difficulty of the blockchain
	InitDifficulty = uint64(5000)
)

var (
	maxHash []byte
)

func init() {
	maxHash, _ = hex.DecodeString("ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff")
}

// CalculateNextDifficulty returns a value that the next block should adhere to.
func CalculateNextDifficulty(block *Protobufs.ProofOfWorkBlock) uint64 {
	previous, err := GetPreviousBlock(block)
	if err != nil {
		return block.Difficulty
	}
	for i := 0; i < 5; i++ {
		previous, err = GetPreviousBlock(previous)
		if err != nil {
			return block.Difficulty
		}
	}
	deltaT := block.Timestamp - previous.Timestamp
	if deltaT == 0 {
		deltaT = 1
	}
	var toReturn uint64 = (block.Difficulty * expectedDelta) / deltaT
	if toReturn == 0 {
		toReturn = 1
	}
	return (toReturn + block.Difficulty) / 2
}

// DifficultyToMaxHashSize gets max hash size, given a difficulty
func DifficultyToMaxHashSize(difficulty uint64) []byte {
	maxHashInt := &big.Int{}
	maxHashInt = maxHashInt.SetBytes(maxHash)

	toRemove := &big.Int{}
	toRemove = toRemove.SetUint64(difficulty)
	glog.Infof("divisor: %b", toRemove)
	maxHashInt = maxHashInt.Div(maxHashInt, toRemove)
	return maxHashInt.Bytes()
}

// HashSmallerThanTarget returns whether a hash is less than a target hash
func HashSmallerThanTarget(hash []byte, target []byte) bool {
	hashInt := &big.Int{}
	hashInt = hashInt.SetBytes(hash)

	targetInt := &big.Int{}
	targetInt = targetInt.SetBytes(target)

	return hashInt.Cmp(targetInt) < 0
}
