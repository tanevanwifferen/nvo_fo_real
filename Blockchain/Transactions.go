package Blockchain

import (
	"../Crypto"
	"../Protobufs"
	"../Storage"
	"../Util"
	"bytes"
	"encoding/hex"
	"github.com/golang/glog"
)

// HashTransaction returns the transaction hash and the signature hash
func HashTransaction(msg *Protobufs.Transaction) (blockHash []byte, sigHash []byte) {
	var inputHash []byte
	for _, input := range msg.Inputs {
		query := input.InputQuery
		inputHash = append(inputHash, query.TransactionHash...)
		inputHash = append(inputHash, []byte(string(query.OutputNumber))...)
		inputHash = Crypto.SafeHash(inputHash)
	}
	var outputHash []byte
	for _, output := range msg.Outputs {
		outputHash = append(outputHash, output.Pubkey...)
		outputHash = append(outputHash, []byte(string(output.Amount))...)
		outputHash = append(outputHash, output.PasswordHash...)
		outputHash = append(outputHash, []byte(string(output.RevertTime))...)
		outputHash = Crypto.SafeHash(outputHash)
	}

	inputHash = append(inputHash, msg.Data...)

	txHash := Crypto.SafeHash(append(inputHash, outputHash...))

	var signhash = msg.Signature
	signhash = Crypto.SafeHash(signhash)
	return txHash, signhash
}

// VerifyTransaction verifies if a transaction is legal. It only works for
// non-coinbase transactions
func VerifyTransaction(msg *Protobufs.Transaction, isCoinbase bool) (verified bool, fees uint64) {
	// hash should be as stated in block
	// msg.transaction should be signed by msg.pubkey
	// inputs should be unspent
	// all inputs should be unlockable by sender
	blockHash, _ := HashTransaction(msg)
	if !bytes.Equal(blockHash, msg.Hash) {
		glog.Info("txhash set != txhash calculated")
		return false, uint64(0)
	}

	amountInInputs := uint64(0)

	// begin check all inputs are unlockable
	for _, input := range msg.Inputs {
		// get output matching tx input
		utxo, err := GetUTXO(input.InputQuery)
		if err != nil {
			glog.Info("utxo not found")
			return false, uint64(0)
		}
		block, _ := GetBlock(utxo.Blockhash)
		var transaction *Protobufs.Transaction
		for _, tx := range block.Transactions {
			if bytes.Equal(tx.Hash, utxo.Txhash) {
				transaction = tx
				break
			}
		}
		// input non existent
		if uint32(len(transaction.Outputs)) < input.InputQuery.OutputNumber {
			glog.Info("Input tx doesn't match query given")
			return false, uint64(0)
		}
		output := transaction.Outputs[input.InputQuery.OutputNumber]
		if bytes.Equal(msg.Pubkey, output.Pubkey) { // redeem case, or simple send
			if len(output.PasswordHash) != 0 {
				// passwordhash must match when set
				if !bytes.Equal(output.PasswordHash, Crypto.SafeHash(input.Password)) {
					glog.Info("password given should match hash with atomic swap")
					return false, uint64(0)
				}
			}
			if len(transaction.Inputs) == 0 {
				latestBlock, err := GetLatestBlock()
				if err == nil {
					if latestBlock.BlockNumber < block.BlockNumber+output.RevertTime {
						glog.Info("Must wait given time to use newly mined coins")
						return false, uint64(0)
					}
				}
			}
		} else if bytes.Equal(msg.Pubkey, transaction.Pubkey) { // revert case
			// can't revert coins that are not in atomic swap
			if len(output.PasswordHash) == 0 {
				glog.Info("Not allowed to spend coins that are not ours")
				return false, uint64(0)
			}

			// check age, err not needed because blockId checked the error
			latest, err := GetLatestBlock()
			if err != nil {
				glog.Info("Not allowed to add transactions in first block")
				return false, uint64(0)
			}
			if output.RevertTime >= latest.BlockNumber-block.BlockNumber {
				glog.Info("Not allowed to revert transactions before wait time has passed")
				return false, uint64(0)
			}
		} else {
			glog.Info("Not allowed to spend coins that are not ours")
			return false, uint64(0)
		}
		amountInInputs = amountInInputs + output.Amount
	}
	// end check inputs are unlockable

	var outputAmount = uint64(0)
	for _, output := range msg.Outputs {
		outputAmount = outputAmount + output.Amount
	}

	if !isCoinbase {
		if amountInInputs < outputAmount {
			glog.Info("more money sent then available")
			return false, uint64(0)
		}
	}

	// check signature
	if Crypto.SafeVerifySignature(blockHash, msg.Signature, msg.Pubkey) != 1 {
		glog.Info("signature fails")
		return false, uint64(0)
	}

	if len(msg.Inputs) > 0 {
		return true, amountInInputs - outputAmount
	}
	return true, uint64(0)
}

// VerifyBlock checks if a block passes the requirements of the chain
func VerifyBlock(block *Protobufs.ProofOfWorkBlock) bool {
	// TODO: sum of all tx inputs should not be more than sum of all outputs
	// TODO: only coinbase transaction can have empty inputs
	// TODO: inputs should be part of blockchain
	// if local copy already exitst
	glog.Infof("verifying block: %s", hex.EncodeToString(block.Hash))
	var localCopy Protobufs.ProofOfWorkBlock
	err := Storage.Read(block.Hash, &localCopy)
	if err == nil {
		// block already exists in storage
		glog.Infof("block: %s already in storage", hex.EncodeToString(block.Hash))
		return false
	}

	prev, err := GetPreviousBlock(block)

	// if previous block exists, but block number is not incremented by one
	if err == nil && prev.BlockNumber+1 != block.BlockNumber {
		glog.Info("block number invalid")
		return false
	}

	// root block with startingdifficulty wrongly set
	if block.BlockNumber == 0 && block.Difficulty != InitDifficulty {
		glog.Info("root difficulty invalid")
		return false
	}

	// non root block with difficulty not valid
	if block.BlockNumber > 0 {
		difficulty := CalculateNextDifficulty(prev)
		if err == nil && difficulty != block.Difficulty {
			glog.Infof("difficulty invalid, given %d, expected %d", block.Difficulty, difficulty)
			return false
		}
	}

	if len(block.Transactions) > 200 {
		glog.Info("too many transactions in block")
		return false
	}

	if len(block.Transactions) > 0 {
		glog.Infof("%d transactions in block", len(block.Transactions))
		feesPaid := uint64(0)
		for index, tx := range block.Transactions {
			verified, fees := VerifyTransaction(tx, index == 0)
			if !verified && block.BlockNumber != 0 {
				glog.Infof("transaction %d fails verification", index)
				return false
			}
			// non base transactions
			if index > 0 {
				if len(tx.Inputs) == 0 {
					glog.Info("only coinbase transaction can have zero inputs")
					return false
				}
			} else {
				// coinbase tx
				if len(tx.Outputs) != 1 {
					glog.Info("coinbase has too few or much outputs")
					return false
				}
				if tx.Outputs[0].RevertTime != 10 {
					glog.Info("coinbase revert time invalid")
					return false
				}

			}
			feesPaid += fees
		}
		coinbase := block.Transactions[0]
		if coinbase.Outputs[0].Amount > feesPaid/2 && block.BlockNumber > 0 {
			glog.Info("coinbase amount too high")
			return false
		}
	}

	hash := Util.HashBlock(block)
	sig := block.Signature
	// signature valid
	if Crypto.VerifySignature(hash, sig, block.PubKey) != 1 {
		glog.Info("signature invalid")
		glog.Info("nonce:" + hex.EncodeToString(block.Nonce))
		glog.Info("hash:" + hex.EncodeToString(hash))
		glog.Info("sig:" + hex.EncodeToString(sig))
		glog.Info("pub:" + hex.EncodeToString(block.PubKey))
		return false
	}
	glog.Info("block valid")
	return true
}
