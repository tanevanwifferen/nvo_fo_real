package Blockchain

import (
	"flag"
	"github.com/golang/glog"
	"testing"
)

func Difficulty(in uint64) {
	maxSize := DifficultyToMaxHashSize(in)
	glog.Infof("max size: %b", maxSize)
}

// Testdifficultyworks is a test that checks wheter the conversion
// from integer to max hash size works
func TestDifficultyWorks(t *testing.T) {
	_ = flag.Set("logtostderr", "true")
	for i := uint64(1); i < 100; i++ {
		Difficulty(i)
	}
	t.Fail()
}
